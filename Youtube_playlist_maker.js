var videos = document.querySelectorAll('yt-icon.ytd-menu-renderer');
var timer = 0;
for (var i = 20; i>0; i--) {
    setTimeout((j)=>{
    	document.querySelectorAll('yt-formatted-string.ytd-menu-service-item-renderer')[0]?.click();
        videos[j].click();
    },timer++,i);
}

/* 2021 Version
// get the videos elements and declare the hover event
var videos = document.querySelectorAll('ytd-grid-video-renderer'),
	hover = new MouseEvent('mouseenter', {'view':window, 'bubbles':true, 'cancelable':true}),
	newVideos=[];
//iterate over the videos and add the playable ones to a new list. stop at the first already watched video
for (let video of videos) {
    if(isNaN(video.querySelector('ytd-thumbnail-overlay-time-status-renderer')?.textContent.trim().charAt(0))) continue;
    if(video.querySelector("#progress")?.style.width==="100%") break;
    newVideos.push(video);
}
//reverse the new list and add videos to the "next to watch" youtube playlist 
for (let video of newVideos.reverse()) {
    setTimeout(()=>{
	    video.querySelector('ytd-thumbnail').dispatchEvent(hover);
	    video.querySelector('ytd-thumbnail-overlay-toggle-button-renderer:nth-child(2) yt-icon').click();
    },0);
}
//when the playlist is ready, start the first video and expand player
setTimeout(()=>{
	document.querySelector('.ytp-play-button').click();
	document.querySelector('.ytp-miniplayer-expand-watch-page-button').click();
}, 1000);
*/
